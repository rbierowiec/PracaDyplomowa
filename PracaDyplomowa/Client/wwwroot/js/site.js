﻿function createAlert(message, type) {
    if ($('.bs-content').length == 0) {
        $('body').prepend('<div style="position: absolute; opacity: 1; z-index: 7000; margin-top: 80px; right: 40px;" class="bs-content"></div>');
    }

    var uniqueId = Date.now();

    $('.bs-content').append('<div id="popup_'+uniqueId+'" class="alert alert-' + type + ' fade in">' +
                                '<a class="close" data-dismiss="alert">&times;</a>' +
                                '<strong>' + message + '</strong>' +
                            '</div>');
                            
    setTimeout(function(){
        $('#popup_'+uniqueId).fadeOut('slow', function(){ $('#popup_'+uniqueId).remove(); });
    },5000);
}

function sessionProtector() {
    var currentPath = window.location.pathname;

    if ((currentPath === "/" || currentPath === "/Index/Rejestracja") && sessionStorage.access_token !== "" && sessionStorage.access_token !== undefined) {
        window.location.href = "/Home/Profile";
    } else if (currentPath !== "/" && currentPath !== "/Index/Rejestracja" && (sessionStorage.access_token === "" || sessionStorage.access_token === undefined)) {
        window.location.href = "/";
    }

    if (sessionStorage.role == 1) {
        $('.dropdown-menu a.manageAdvertisements').parent().remove();
    }
}

function convertDateTimeToDate(dateTime) {
    if (dateTime != null && dateTime != "") {
        var d1 = new Date(dateTime);
        var year = d1.getFullYear();
        var month = ("0" + (parseInt(d1.getMonth()) + 1)).slice(-2);
        var day = ("0" + d1.getDate()).slice(-2);

        return year + "-" + month + "-" + day;
    } else {
        return "";
    }
}

$(document).ready(function () {
    // Function check if user got prvilage to access page
    sessionProtector();

    var tooltipText = "Field is required";

    $('input[required=true]').each(function () {
        if ($(this).val() == "") {
            $(this).prop('title', tooltipText).tooltip({
                placement: 'right'
            });
        }
    })

    $('input[required=true]').on('keyup change', function () {
        if ($(this).val().length == 0) {
            $(this).tooltip('hide')
                   .attr('data-original-title', tooltipText)
                   .tooltip('fixTitle');
        } else {
            $(this).tooltip('hide')
                   .attr('data-original-title', '')
                   .tooltip('fixTitle');
        }
    })

    $('.logoutButton').click(function () {
        var i = sessionStorage.length;
        while (i--) {
            var key = sessionStorage.key(i);
            sessionStorage.removeItem(key);
        }
    });
})