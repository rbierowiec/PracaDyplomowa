﻿function rejestracja(serviceUrl, email, password) {
    var ajaxData = new Object();
    ajaxData['password'] = password;
    ajaxData['email'] = email;
    ajaxData['confirmPassword'] = password;

    $.ajax({
        url: serviceUrl,
        method: "POST",
        async: false,
        dataType: 'json',
        data: ajaxData,
        beforeSend: function (xhr) { xhr.setRequestHeader('Accept', 'application/json'); },
        success: function (data) {
            createAlert('Pomyślnie założono konto!', 'success');
        },
        error: function (xhr) {
            if (xhr.status == 200) {
                var serviceUrl2 = "http://localhost:50390/api/Users"
                uzupelnijDodatkoweDane(serviceUrl2, email);

                createAlert('Pomyślnie założono konto!', 'success');
            } else if (xhr.status == 400) {
                var error = JSON.parse(xhr.responseText);
                var errorMessages = error.ModelState;
                errorMessages = errorMessages[Object.keys(errorMessages)[0]];

                $.each(errorMessages, function (key, val) {
                    createAlert(val, 'danger');
                });
            } else if (xhr.status == 500) {
                rejestracja(serviceUrl, email, password);
            }
        }
    });
}

function uzupelnijDodatkoweDane(serviceUrl, email) {
    var ajaxData = new Object();
    ajaxData['email'] = email;
    console.log(serviceUrl, email);

    $.ajax({
        url: serviceUrl,
        method: "POST",
        async: false,
        dataType: 'json',
        data: ajaxData,
        //beforeSend: function (xhr) { xhr.setRequestHeader('Content-Type', 'application/json'); },
        success: function (data) {
            console.log(data);
            //createAlert('Pomyślnie założono konto!', 'success');
        },
        error: function (xhr) {
            //console.log(xhr);
            //if (xhr.status == 200) {
            //    createAlert('Pomyślnie założono konto!', 'success');
            //} else if (xhr.status == 400) {
            //var error = JSON.parse(xhr.responseText);
            console.log(xhr);
            //    var errorMessages = error.ModelState;
            //    errorMessages = errorMessages[Object.keys(errorMessages)[0]];

            //    $.each(errorMessages, function (key, val) {
            //        createAlert(val, 'danger');
            //    });
            //} else if (xhr.status == 500) {
            //    rejestracja(serviceUrl, email, password);
            //}
        }
    });
}

$(document).ready(function () {
    $('button.btn-register').on('click', function () {
        var serviceUrl = "http://localhost:56933/api/Account/Register";
        var email = $('input[name=email]').val();
        var password = $('input[name=password]').val();

        if (email != "" && password != "") {
            rejestracja(serviceUrl, email, password);
        } else {
            if (email.length == 0) {
                $('input[name=email]').tooltip('show');
            }

            if (password.length == 0) {
                $('input[name=password]').tooltip('show');
            }
        }
    });
});