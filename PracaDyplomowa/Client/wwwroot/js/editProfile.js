﻿function loadProfile() {
    serviceUrl = "http://localhost:50390/api/Users";

    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        dataType: 'json',
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            $.each(data, function (key, value) {
                if ($('input[name=' + key + ']').length > 0) {
                    $('input[name=' + key + ']').val(value).change();
                }
            });
        },
        error: function (xhr) {
        }
    });
}

function loadEducationAndWorkExperience() {
    serviceUrl = "http://localhost:50390/api/UsersExperience/" + sessionStorage.idUser;

    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        dataType: 'json',
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            $.each(data, function (key, value) {
                if (value["type"] == 1) {
                    $('div.education').append(prepareEducationBox(value));
                } else if(value["type"] == 2){
                    $('div.work-experience').append(prepareWorkExperieneBox(value));
                }
            });
        },
        error: function (xhr) {
        }
    });
}

function saveProfile() {
    serviceUrl = "http://localhost:50390/api/Users/" + sessionStorage.idUser;
    var data = new Object();

    $('input.form-control').each(function () {
        if ($(this).attr('name') != undefined && $(this).attr('name') != "") {
            data[$(this).attr('name')] = $(this).val();
        }
    })

    var isEmptyInput = false;

    // Sprawdz, czy wymagane pola są wypełnione
    $('.profile input[required=true]').each(function () {
        if ($(this).val() == "") {
            $(this).tooltip("show");
            isEmptyInput = true;
        }
    })

    if (isEmptyInput) return false;

    data['Id'] = sessionStorage.idUser;
    $.ajax({
        url: serviceUrl,
        method: "PUT",
        async: false,
        dataType: 'json',
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: data,
        success: function (data) {
            createAlert("Data saved successfully", 'success');
        },
        error: function (xhr) {
        }
    });
}

function saveExperience(expid) {
    var expid = $('.list-group-item.list-group-item-action.editing').attr('expid');
    var data = getDataFromEditingBox();
    serviceUrl = "http://localhost:50390/api/UsersExperience";

    if (expid == 0) {
        var newItemId = 0;
        $.ajax({
            url: serviceUrl,
            method: "POST",
            async: false,
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.access_token,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data,
            success: function (data) {
                createAlert("Data saved successfully", 'success');
                newItemId = data['id'];
            },
            error: function (xhr) {
            }
        });

        $('.list-group-item.list-group-item-action.editing').replaceWith(parseEditingBoxToData());
        $('.list-group-item.list-group-item-action[expid=undefined]').attr('expid', newItemId);
    } else {
        serviceUrl += "/" + expid;
        data['Id'] = expid;

        $.ajax({
            url: serviceUrl,
            method: "PUT",
            async: false,
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.access_token,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data,
            success: function (data) {
                createAlert("Data saved successfully", 'success');
            },
            error: function (xhr) {
            }
        });

        $('.list-group-item.list-group-item-action.editing').replaceWith(parseEditingBoxToData());
        $('.list-group-item.list-group-item-action[expid=undefined]').attr('expid', expid);
    }
}

function removeExperience(expid) {
    serviceUrl = "http://localhost:50390/api/UsersExperience/"+expid;

    $.ajax({
        url: serviceUrl,
        method: "DELETE",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        success: function (data) {
            createAlert("Data removed successfully", 'success');
            $('.list-group-item.list-group-item-action.editing').remove();
        },
        error: function (xhr) {
        }
    });
}

function prepareEducationBox(data) {
    str = "<div class='list-group-item list-group-item-action' type='1' expId='" + data['Id'] + "'>";
    str += "<p class='list-group-item-text placeName'><b>" + returnString(data["placeName"]) + "</b></p>";
    str += "<p class='list-group-item-text description'>" + returnString(data['Position']) + "</p>";
    str += "<p class='list-group-item-text dates'>" + returnString(data['startDate']) + " - " + returnString(data['endDate']) + "</p>";
    str += "<p class='list-group-item-text description'>" + returnString(data['Description']) + "</p>";
    str += "</div>";

    return str;
}

function prepareWorkExperieneBox(data) {
    str = "<div class='list-group-item list-group-item-action' type='2' expId='"+data['Id']+"'>";
    str += "<p class='list-group-item-text placeName'><b>" + returnString(data["placeName"]) + "</b></p>";
    str += "<p class='list-group-item-text description'>" + returnString(data['Position']) + "</p>";
    str += "<p class='list-group-item-text dates'>" + returnString(data['startDate']) + " - " + returnString(data['endDate']) + "</p>";
    str += "<p class='list-group-item-text description'>" + returnString(data['Description']) + "</p>";
    str += "</div>";

    return str;
}

function transformEducationBoxIntoEditBox(htmlBox, expid) {
    var data = parseHtmlBoxToData(htmlBox);
    str = "<div class='list-group-item list-group-item-action editing' type='1' expId='"+expid+"'>";
    str += "<p class='list-group-item-text placeName'><input placeholder='Place name' name='placeName' class='form-control' value='" + returnString(data["placeName"]) + "' /></p>";
    str += "<p class='list-group-item-text position'><input name='Position' placeholder='Field of study' class='form-control' value='" + returnString(data["Position"]) + "' /></p>";
    str += "<p class='list-group-item-text dates' style='display: inline-flex; width: 100%;'><input name='startDate' placeholder='Start date' class='form-control' value='" + returnString(data["startDate"]) + "' /><input name='endDate' placeholder='End date' class='form-control' value='" + returnString(data["endDate"]) + "' /></p>";
    str += "<p class='list-group-item-text description'><input name='Description' placeholder='Description' class='form-control' value='" + returnString(data["Description"]) + "' /></p>";
    str += "<button class='btn btn-primary save'>Save</button><span> </span>";
    str += "<button class='btn btn-danger remove'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Remove</button>";
    str += "</div>";

    return str;
}

function transformWorkExperienceBoxIntoEditBox(htmlBox, expid) {
    var data = parseHtmlBoxToData(htmlBox);
    str = "<div class='list-group-item list-group-item-action editing' type='2' expId='" + expid + "'>";
    str += "<p class='list-group-item-text placeName'><input placeholder='Place name' name='placeName' class='form-control' value='" + returnString(data["placeName"]) + "' /></p>";
    str += "<p class='list-group-item-text position'><input name='Position' placeholder='Position' class='form-control' value='" + returnString(data["Position"]) + "' /></p>";
    str += "<p class='list-group-item-text dates' style='display: inline-flex; width: 100%;'><input name='startDate' placeholder='Start date' class='form-control' value='" + returnString(data["startDate"]) + "' /><input name='endDate' placeholder='End date' class='form-control' value='" + returnString(data["endDate"]) + "' /></p>";
    str += "<p class='list-group-item-text description'><input name='Description' placeholder='Description' class='form-control' value='" + returnString(data["Description"]) + "' /></p>";
    str += "<button class='btn btn-primary save'>Save</button><span> </span>";
    str += "<button class='btn btn-danger remove'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Remove</button>";
    str += "</div>";

    return str;
}

function parseHtmlBoxToData(htmlBox) {
    var object = $('<div/>').html(htmlBox).contents();
    var data = new Object();
    console.log(object);
    data["placeName"] = object[0].innerText;
    data["Position"] = object[1].innerText;

    var dates = object[2].innerText.split(' - ');
    data["startDate"] = dates[0];
    data["endDate"] = dates[1];
    data["Description"] = object[3].innerText;

    return data;
}

function parseEditingBoxToData() {
    var data = getDataFromEditingBox();
    var type = $('.list-group-item.list-group-item-action.editing').attr('type');

    if (type == '1') {
        return prepareEducationBox(data);
    } else if (type == '2') {
        return prepareWorkExperieneBox(data);
    }
}

function getDataFromEditingBox() {
    var data = new Object();

    $('.list-group-item.list-group-item-action.editing input').each(function () {
        data[$(this).attr('name')] = $(this).val();
    })

    data['Type'] = $('.list-group-item.list-group-item-action.editing').attr('type');
    return data;
}

function returnString(string) {
    return ((string == null) ? "" : string);
}

$(document).ready(function () {
    loadProfile();
    loadEducationAndWorkExperience();

    $('input[name=save]').click(function () { saveProfile(); });
    $('body').on('dblclick', '.list-group-item.list-group-item-action', function () {
        if ($('.list-group-item.list-group-item-action.editing').length == 0) {
            if ($(this).attr('type') == '1') {
                $(this).replaceWith(transformEducationBoxIntoEditBox($(this).html(), $(this).attr('expid')));
            } else if ($(this).attr('type') == '2') {
                $(this).replaceWith(transformWorkExperienceBoxIntoEditBox($(this).html(), $(this).attr('expid')));
            }
        }
    })

    $('body').on('click', '.list-group-item.list-group-item-action.editing button.save', function () {
        saveExperience();
    })

    $('body').on('click', '.list-group-item.list-group-item-action.editing button.remove', function () {
        removeExperience($(this).parent().attr('expid'));
    })

    $('body').on('click', '.addEducation, .addExperience', function () {
        var emptyObject = new Object();
        emptyObject['Id'] = 0;
        emptyObject['placeName'] = "";
        emptyObject['Description'] = "";
        emptyObject['Position'] = "";
        emptyObject['startDate'] = "";
        emptyObject['endDate'] = "";

        if ($(this).attr('class').indexOf('addEducation') >= 0) {
            emptyObject['Type'] = 1;
            $('div.education').append(prepareEducationBox(emptyObject));
            var newItem = $('div.education .list-group-item.list-group-item-action[expid=0]');
            newItem.replaceWith(transformEducationBoxIntoEditBox(newItem.html(), 0));
        } else {
            emptyObject['Type'] = 2;
            $('div.work-experience').append(prepareWorkExperieneBox(emptyObject));
            var newItem = $('div.work-experience .list-group-item.list-group-item-action[expid=0]');
            newItem.replaceWith(transformWorkExperienceBoxIntoEditBox(newItem.html(), 0));
        }
    })
})