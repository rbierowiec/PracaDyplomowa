﻿function loadApplications() {
    serviceUrl = "http://localhost:50390/api/user/applications";

    var applications = [];

    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            index = 0;
            $.each(data, function (key, value) {
                appendApplicationsToBody(value, ++index);
            });
        },
        error: function (xhr) {
        }
    });

    return applications;
}

function appendApplicationsToBody(app, index) {
    str = "<tr class='"+getClassForStatus(app['status'])+"'>";
        str += "<td>" + index + "</td>";
        str += "<td>" + app['advertisementTitle'] + "</td>";
        str += "<td>" + convertDateTimeToDate(app['dateOfApply']) + "</td>";
        str += "<td>" + mapStatus(app['status']) + "</td>";
        if (app['status'] == 2) {
            str += "<td><button class='btn btn-danger cancelApplication' value='" + app['Id'] + "'>Cancel</button></td>";
        } else {
            str += "<td></td>";
        }
    str += "</tr>";

    $('table.applications tbody').append(str);
}

function mapStatus(status) {
    switch (status) {
        case 1: return "Accepted";
        case 2: return "Waiting for response";
        case 3: return "Rejected";
        case 4: return "Canceled by owner";
        default: return "Not found";
    }
}

function getClassForStatus(status){
    switch (status) {
        case 1: return "applicationAccepted";
        case 2: return "applicationWaiting";
        case 3: return "applicationRejected";
        case 4: return "applicationCancelled";
        default: return "applicationCancelled";
    }
}

function cancelApplication(appId) {
    var currentRow = $('button.cancelApplication[value=' + appId + ']').parent().parent();

    serviceUrl = "http://localhost:50390/api/applications/" + appId;
    var data = {};
    data['status'] = 4;

    $.ajax({
        url: serviceUrl,
        method: "PUT",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        data: data,
        success: function (data) {
            currentRow.addClass('applicationCancelled');
            currentRow.find('.cancelApplication').remove();
            currentRow.children().eq(3).html(mapStatus(4));
            createAlert("Application cancelled successfully", "success");
        },
        error: function (xhr) {
            createAlert(JSON.parse(xhr.responseText).msg, "danger");
        }
    });
}

$(document).ready(function () {
    loadApplications();

    $('body').on('click', '.cancelApplication', function () {
        var appId = $(this).val();

        bootbox.dialog({
            message: "Are you sure you want to Cancel ?",
            title: "<i class='glyphicon glyphicon-trash'></i> Cancel !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success",
                    callback: function () {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Cancel!",
                    className: "btn-danger",
                    callback: function () {
                        cancelApplication(appId);
                    }
                }
            }
        });

    })
});