﻿function loadAdvertisements() {
    serviceUrl = "http://localhost:50390/api/Notices";

    var advertisments = [];

    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            advertisements = data;
        },
        error: function (xhr) {
        }
    });

    return advertisements;
}

function showAdvertisements(adverts) {
    $.each(adverts, function (key, value) {
        $('div.advertisements').append(prepareAdvertisementBox(value));
    });
}



function prepareAdvertisementBox(data) {
    str = "<div class='advertisement' advertId='" + data['Id'] + "'>";
    str += "<div class='content'>";
    str += "<div class='title'>" + data['Title'] + "</div>";
    str += "<div class='author'>Author: " + data['Author'] + "</div>";
    str += "<div class='publicationDate'>Publication date: " + convertDateTimeToDate(data['CreationDate']) + "</div>";
    str += "</div>";
    str += "</div>";

    return str;
}

function prepareAdvertisementDetails(advertisementId) {
    serviceUrl = "http://localhost:50390/api/Notices/"+advertisementId;
    var advertisement;

    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            advertisement = data;
        },
        error: function (xhr) {
            console.log(xhr);
        }
    });

    str = "<div class='advertisementDetails' advertId='" + advertisementId + "'>";
        str += "<div class='projectAim detail'>";
            str += "<div class='contentHeader'>Project aim:</div>";
            str += "<div class='contentBody'>" + returnString(advertisement['ProjectAim']) + "</div>";
        str += "</div>";
        str += "<div class='tasks detail'>";
            str += "<div class='contentHeader'>Tasks to do:</div>";
            str += "<div class='contentBody'>" + returnString(advertisement['Tasks']) + "</div>";
        str += "</div>";
        str += "<div class='requiredSkills detail'>";
            str += "<div class='contentHeader'>Required skills:</div>";
            str += "<div class='contentBody'>" + returnString(advertisement['RequiredSkills']) + "</div>";
        str += "</div>";
        str += "<div class='listDetails'>";
            str += "<div class='projectLength item'>";
                str += "<div class='contentHeader'>Project length (in days):</div>";
                str += "<div class='contentBody'>" + returnString(advertisement['ProjectLength']) + "</div>";
            str += "</div>";
            str += "<div class='publicationDate item'>";
                str += "<div class='contentHeader'>Publication date:</div>";
                str += "<div class='contentBody'>" + convertDateTimeToDate(advertisement['CreationDate']) + "</div>";
            str += "</div>";
        str += "</div>";
        str += "<div style='display: none;' class='userComment'>";
            str += "<div class='contentHeader'>Your motivation:</div>";
            str += "<div class='contentBody'><textarea name='userMotivation'></textarea></div>";
        str += "</div'>";
    str += "</div>";

    return str;
}

function showAdvertisementDetails(advertisementId) {
    BootstrapDialog.show({
        title: "Details of advertisement",
        message: prepareAdvertisementDetails(advertisementId),
        cssClass: "info",
        buttons: [
            {
                label: 'Apply',
                cssClass: 'btn-success applyButton',
                action: function (dialogRef) {
                    serviceUrl = "http://localhost:50390/api/Applications";
                    var data = {};

                    data['advertisementId'] = advertisementId;
                    data['Motivation'] = $('textarea[name=userMotivation]').val();

                    if (data['Motivation'].length >= 10) {
                        $.ajax({
                            url: serviceUrl,
                            method: "POST",
                            async: false,
                            headers: {
                                'Authorization': 'Bearer ' + sessionStorage.access_token
                            },
                            data: data,
                            success: function (data) {
                                createAlert("Application send successfully", "success");
                                dialogRef.close();
                            },
                            error: function (xhr) {
                                createAlert(JSON.parse(xhr.responseText).msg, "danger");
                            }
                        });
                    } else {
                        createAlert("Motivation field is required to have more than 10 letters", "danger");
                    }
                }
            },
            {
                label: 'Close',
                action: function (dialogRef) {
                    dialogRef.close();
                }
            }
        ]

    });
}

function returnString(string) {
    return ((string == null) ? "" : string);
}

$(document).ready(function () {
    showAdvertisements(loadAdvertisements());

    $('.advertisement').on('click', function () {
        var advertId = $(this).attr('advertId');
        showAdvertisementDetails(advertId);
    })

    $('body').on('mouseenter','.applyButton', function () {
        $('.userComment').slideDown(1000);
    })
});