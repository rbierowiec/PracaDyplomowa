﻿function zaloguj(serviceUrl, email, password) {
    var ajaxData = new Object();
    ajaxData['password'] = password;
    ajaxData['username'] = email;
    ajaxData['grant_type'] = 'password';

    $.ajax({
        url: serviceUrl,
        method: "POST",
        async: false,
        dataType: 'json',
        data: ajaxData,
        beforeSend: function (xhr) { xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); },
        success: function (data) {
            sessionStorage.access_token = data['access_token'];
            var serviceUrl2 = "http://localhost:50390/api/Users";
            pobierzDaneUzytkownika(serviceUrl2);
        },
        error: function (xhr) {
            var response = JSON.parse(xhr.responseText);

            if (response['error_description'].length > 0) {
                createAlert(response['error_description'], 'danger');
            } else {
                sessionStorage.access_token = response['access_token'];
                pobierzDaneUzytkownika(serviceUrl2);
            }
        }
    });
}

function pobierzDaneUzytkownika(serviceUrl) {
    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        dataType: 'json',
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            sessionStorage.idUser = data['Id'];
            sessionStorage.role = data['Role'];
            window.location.href = "home/profile";
        },
        error: function (xhr) {
        }
    });

    //return returnData;
}

function zalogujUzytkownika(serviceUrl, sesja) {
    $.each(sesja, function (key, value) {
        sessionStorage.setItem(key, value);
    });

    window.location = "home/profile";
}

$(document).ready(function () {
    if (sessionStorage["access_token"] != undefined) {
        window.location = "home/profile";
    }

    $('button.login').on('click', function () {
        var serviceUrl = "http://localhost:56933/Token";
        var email = $('input[name=email]').val();
        var password = $('input[name=password]').val();

        if (email != "" && password != "") {
            zaloguj(serviceUrl, email, password);
        } else {
            if (email.length == 0) {
                $('input[name=email]').tooltip('show');
            }

            if (password.length == 0) {
                $('input[name=password]').tooltip('show');
            }
        }
    });
});