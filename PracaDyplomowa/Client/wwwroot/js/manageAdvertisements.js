﻿function loadAdvertisements(userId) {
    serviceUrl = "http://localhost:50390/api/Notices";

    if (userId != null) {
        serviceUrl = "http://localhost:50390/api/user/Advertisements";
    }
    var advertisments = [];

    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            advertisements = data;
        },
        error: function (xhr) {
        }
    });

    return advertisements;
}

function showMyAdvertisements(adverts) {
    $.each(adverts, function (key, value) {
        $('div.advertisements').append(prepareAdvertisementBox(value));
    });
}

function prepareAdvertisementBox(data) {
    str = "<div class='list-group-item list-group-item-action' advertId='"+data['Id']+"'>";
        str += "<p class='list-group-item-text title'><b>" + data['Title'] + "</b></p>";
        str += "<div class='advertisementDetails'>";
            str += "<div class='projectAim detail'>";
                str += "<div class='contentHeader'>Project aim:</div>";
                str += "<div class='contentBody'>" + returnString(data['ProjectAim']) + "</div>";
            str += "</div>";
            str += "<div class='tasks detail'>";
                str += "<div class='contentHeader'>Tasks to do:</div>";
                str += "<div class='contentBody'>" + returnString(data['Tasks']) + "</div>";
            str += "</div>";
            str += "<div class='requiredSkills detail'>";
                str += "<div class='contentHeader'>Required skills:</div>";
                str += "<div class='contentBody'>" + returnString(data['RequiredSkills']) + "</div>";
            str += "</div>";
            str += "<div class='listDetails'>";
                str += "<div class='projectLength item'>";
                    str += "<div class='contentHeader'>Project length (in days):</div>";
                    str += "<div class='contentBody'>" + returnString(data['ProjectLength']) + "</div>";
                str += "</div>";
                str += "<div class='publicationDate item'>";
                    str += "<div class='contentHeader'>Publication date:</div>";
                    str += "<div class='contentBody'>" + convertDateTimeToDate(data['CreationDate']) + "</div>";
            str += "</div>";
            str += "</div>";
        str += "<button class='btn btn-primary showApplications'>Show applications</button><span> </span>";
        str += "<button class='btn btn-primary edit'>Edit</button><span> </span>";
        str += "<button class='btn btn-danger remove'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>";
    str += "</div>";
    str += "</div>";

    return str;
}

function prepareEditForm(data) {
    var regex = /<br\s*[\/]?>/gi;

    if(data == null){
        data = new Object();
        data['Title'] = "";
        data['Length'] = 0;
        data['ProjectAim'] = "";
        data['RequiredSkills'] = "";
        data['Tasks'] = "";
    }
    var advertId = ((data == null || data['Id'] == null) ? 0 : data['Id']);

    str = "<div class='editAdvertisementForm' advertId='"+advertId+"'>";
        str += "<table>";
            str += "<tr>";
                str += "<td>Title</td>";
                str += "<td><input name='title' value='"+returnString(data['Title'])+"'/></td>";
            str += "</tr>";
            str += "<tr>";
                str += "<td>Project length (days)</td>";
                str += "<td><input type='number' name='projectLength' value='"+returnString(data['ProjectLength'])+"'/></td>";
            str += "</tr>";
                str += "<tr><td colspan=2>Aim of the project</td></tr>";
            str += "<tr>";
            str += "<td colspan=2><textarea name='projectAim'>" + returnString(data['ProjectAim']).replace(regex, "&#13;&#10;") + "</textarea></td>"
            str += "</tr>";
            str += "<tr><td colspan=2>Required skills</td></tr>";
            str += "<tr>";
            str += "<td colspan=2><textarea name='requiredSkills'>" + returnString(data['RequiredSkills']).replace(regex, "&#13;&#10;") + "</textarea></td>"
            str += "</tr>";
            str += "<tr><td colspan=2>Tasks to do</td></tr>";
            str += "<tr>";
            str += "<td colspan=2><textarea name='tasks'>" + (returnString(data['Tasks']).replace(regex, "&#13;&#10;")) + "</textarea></td>"
            str += "</tr>";
        str += "</table>";
    str += "</div>";

    return str;
}

function saveAdvertisement(advertisementId, data, dialogRef){
    serviceUrl = "http://localhost:50390/api/Notices";
    var ajaxMethod = "POST";

    if(advertisementId > 0){
        data['Id'] = advertisementId;
        serviceUrl += "/" + advertisementId;
        ajaxMethod = "PUT";
    }
    $.ajax({
        url: serviceUrl,
        method: ajaxMethod,
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: data,
        success: function (response) {
            createAlert("Data saved successfully", 'success');

            if(advertisementId == 0){
                data['Id'] = 0;
                
                $('div.advertisements').append(prepareAdvertisementBox(data));
                $('div.advertisements .list-group-item[advertid=0]').attr('advertid', response.id);
            }else{
                $('div.advertisements div.list-group-item[advertId="'+advertisementId+'"]').replaceWith(prepareAdvertisementBox(data));
            }
            dialogRef.close();
        },
        error: function (xhr) {

            createAlert(JSON.parse(xhr.responseText).msg, "danger");
        }
    });

}

function removeAdvertisement(advertisementId){
    serviceUrl = "http://localhost:50390/api/Notices/" + advertisementId;

    $.ajax({
        url: serviceUrl,
        method: "DELETE",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        success: function (data) {
            createAlert("Item deleted successfully", 'success');
            $('div.advertisements div.list-group-item[advertId="'+advertisementId+'"]').remove();
        },
        error: function (xhr) {
            createAlert(JSON.parse(xhr.responseText).msg, "danger");
        }
    });
}

function returnString(string) {
    return ((string == null) ? "" : string);
}

function prepareBootstrapDialog(data, className){

    if(className == "success"){
        var title = "New advertisement";
    }else if(className == "info"){
        var title = "Edit advertisement";
    }

    BootstrapDialog.show({
        title: title,
        message: prepareEditForm(data),
        cssClass: className,
        buttons: [
            {
                label: 'Cancel',
                action: function(dialogRef){
                    dialogRef.close();
                }
            },
            {
                label: 'Save',
                action: function(dialogRef){
                    var data2 = new Object();
                    data2['Title'] = $('div.editAdvertisementForm input[name="title"]').val();
                    data2['ProjectLength'] = $('div.editAdvertisementForm input[name="projectLength"]').val();
                    data2['ProjectAim'] = $('div.editAdvertisementForm textarea[name="projectAim"]').val().replace(/\n\r?/g, '<br />');
                    data2['RequiredSkills'] = $('div.editAdvertisementForm textarea[name="requiredSkills"]').val().replace(/\n\r?/g, '<br />');
                    data2['Tasks'] = $('div.editAdvertisementForm textarea[name="tasks"]').val().replace(/\n\r?/g, '<br />');
                    data2['CreationDate'] = data['CreationDate'];
                    
                    var advertId = $('div.editAdvertisementForm').attr('advertId');

                    var error = false;

                    $.each(data2, function (key, value) {
                        if(value == ""){
                            createAlert("All fields are required", "danger");
                            error = true;
                            return false;
                        }
                    });

                    if(!error){
                        saveAdvertisement(advertId, data2, dialogRef);
                    }
                }
            }
        ]
        
    });
}

function loadApplications(advertisementId) {
    serviceUrl = "http://localhost:50390/api/notices/applications/" + advertisementId;

    var applications = [];

    $.ajax({
        url: serviceUrl,
        method: "GET",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        success: function (data) {
            applications = data;
        },
        error: function (xhr) {
        }
    });

    return applications;
}

function prepareListOfApplications(applications) {
    str = "<table class='table table-striped applications'>" +
            "<thead>" +
                "<tr>" +
                    "<th style='width: 10%;'>#</th>" +
                    "<th style='width: 20%;'>Applicant name</th>" +
                    "<th style='width: 20%;'>Date of application</th>" +
                    "<th style='width: 30%;'></th>" +
                "</tr>" +
            "</thead>" +
            "<tbody style='max-height: 400px;'>";

    var index = 0;
    $.each(applications, function (key, value) {
        str += "<tr class='"+mapStatusToClass(value['status'])+"' applicationId='"+value['Id']+"'>";
            str += "<td>"+(++index)+"</td>";
            str += "<td>"+value['applicantName']+"</td>";
            str += "<td>" + value['dateOfApply'] + "</td>";
            str += "<td>";
            if (value['status'] == 2) {
                str += "<button class='btn btn-success acceptApplication'>Accept</button><span> </span>";
                str += "<button class='btn btn-danger rejectApplication'>Reject</button><span> </span>";
            } else {
                str += mapStatusToString(value['status']);
            }
            str += "</td>";

        str += "</tr>";
    });

    if (index == 0) {
        str += "<tr>";
            str += "<td colspan=4>No applications available</td>";
        str += "</tr>";
    }

    str +=  "</tbody>" +
        "</table>";
    return str;
}

function showApplications(advertisementId) {
    var applications = loadApplications(advertisementId);


    BootstrapDialog.show({
        title: "Applications",
        cssClass: "info applications",
        message: prepareListOfApplications(applications),
        replaceWith: "800px",
        buttons: [
            {
                label: 'Cancel',
                action: function (dialogRef) {
                    dialogRef.close();
                }
            }
        ]

    });
}

function mapStatusToClass(status) {
    switch (status) {
        case 1: return "accepted";
        case 2: return "waiting";
        case 3: return "rejected";
        case 4: return "cancelled";
        default: return "cancelled";
    }
}

function mapStatusToString(status) {
    switch (status) {
        case 1: return "Accepted";
        case 2: return "Waiting";
        case 3: return "Rejected";
        case 4: return "Cancelled by owner";
        default: return "Not found";
    }
}

function responseApplication(applicationId, status) {
    serviceUrl = "http://localhost:50390/api/applications/" + applicationId;
    var currentRow = $('tr[applicationid=' + applicationId + ']');

    var data = {};
    data['status'] = status;

    $.ajax({
        url: serviceUrl,
        method: "PUT",
        async: false,
        headers: {
            'Authorization': 'Bearer ' + sessionStorage.access_token
        },
        data: data,
        success: function (data) {
            currentRow.addClass(mapStatusToClass(status));
            currentRow.children().last().html(mapStatusToString(status));
            //currentRow.addClass('applicationCancelled');
            //currentRow.find('.cancelApplication').remove();
            //currentRow.children().eq(3).html(mapStatus(4));
            //createAlert("Application cancelled successfully", "success");
        },
        error: function (xhr) {
            createAlert(JSON.parse(xhr.responseText).msg, "danger");
        }
    });
}

$(document).ready(function () {
    showMyAdvertisements(loadAdvertisements(sessionStorage.idUser));

    $('body').on('click', 'div.advertisements div.list-group-item p.title', function () {
        var thisAdvert = $(this).parent();

        $('div.advertisementDetails').slideUp();
        if (!thisAdvert.children('.advertisementDetails').is(':visible')) {
            thisAdvert.children('.advertisementDetails').slideDown();
        }
    });

    $('body').on('click', 'button.addAdvertisement', function () {
        prepareBootstrapDialog(null, "success");
    });

    $('body').on('click', 'button.edit', function(){
        var itemContent = $(this).parent().parent();
        var advertisementId = itemContent.attr('advertId');

        serviceUrl = "http://localhost:50390/api/Notices/" + advertisementId;
        var advertisement;

        $.ajax({
            url: serviceUrl,
            method: "GET",
            async: false,
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.access_token
            },
            success: function (data) {
                advertisement = data;
            },
            error: function (xhr) {
            }
        });

        prepareBootstrapDialog(advertisement, "info");
    });

    $('body').on('click', 'button.remove', function(){
        var itemContent = $(this).parent().parent();
        removeAdvertisement(itemContent.attr('advertId'));
    })

    $('body').on('click', 'button.showApplications', function () {
        var itemContent = $(this).parent().parent();
        var advertisementId = itemContent.attr('advertId');

        showApplications(advertisementId);
    })

    $('body').on('click', 'button.acceptApplication', function () {
        var applicationId = $(this).parent().parent().attr('applicationid');

        bootbox.dialog({
            message: "Are you sure you want to accept that application ?",
            title: "<i class='glyphicon glyphicon-question-sign'></i> Acceptance",
            buttons: {
                success: {
                    label: "Accept",
                    className: "btn-success",
                    callback: function () {
                        responseApplication(applicationId, 1);
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "btn-primary",
                    callback: function () {
                        $('.bootbox').modal('hide');
                    }
                }
            }
        });
    })

    $('body').on('click', 'button.rejectApplication', function () {
        var applicationId = $(this).parent().parent().attr('applicationid');

        bootbox.dialog({
            message: "Are you sure you want to reject that application ?",
            title: "<i class='glyphicon glyphicon-question-sign'></i> Rejection",
            buttons: {
                success: {
                    label: "Reject",
                    className: "btn-danger",
                    callback: function () {
                        responseApplication(applicationId,3);
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "btn-primary",
                    callback: function () {
                        $('.bootbox').modal('hide');
                    }
                }
            }
        });
    })
})