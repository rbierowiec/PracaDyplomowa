﻿public class SessionValues
{
    public string UserId { get; set; }
    public string Email { get; set; }
    public string LoggedIn { get; set; }
}