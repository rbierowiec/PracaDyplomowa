﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Client.Controllers
{
    public class IndexController : Controller
    {
        public IActionResult Logowanie()
        {
            return View();
        }

        public IActionResult Rejestracja()
        {
            return View();
        }
    }
}
