﻿using System.Collections.Generic;

namespace Library
{
    public class User
    {
        // Setting default value for Role
        public User()
        {
            Role = 1;
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Role { get; set; }
    }

    public class Roles
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
    }

    public class Advertisement {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string Title { get; set; }
        public string ProjectAim { get; set; }
        public string RequiredSkills { get; set; }
        public string Tasks { get; set; }
        public int ProjectLength { get; set; }
        public string CreationDate { get; set; }
    }

    public class Experience
    {
        public int Id { get; set; }
        public int userId { get; set; }
        public string placeName { get; set; }
        // Dla pracy - pozycja, dla szkoły - kierunek
        public string Position { get; set; }
        public string Description { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        // 1 - szkoła, 2 - doświadczenie zawodowe
        public int type { get; set; }
    }

    public class Application{
        public int Id { get; set; }
        public int advertisementId { get; set; }
        public int userId { get; set; }
        public string Motivation { get; set; }
        public string dateOfApply { get; set; }
        // 1 - zaakceptowany, 2 - złożony, 3 - anulowany (przez składającego)
        public int status { get; set; }
    }
}
