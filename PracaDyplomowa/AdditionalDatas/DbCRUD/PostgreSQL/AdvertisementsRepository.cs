﻿using AdditionalDatas.Contexts;
using AdditionalDatas.Interfaces;
using Library;
using System.Collections.Generic;
using System.Data.Entity;

namespace AdditionalDatas.DbCRUD.PostgreSQL
{
    public class AdvertisementsRepository : IAdvertisementsRepository
    {
        private MyContext db = new MyContext();

        public int AddAdvertisement(Advertisement advertisement)
        {
            db.Advertisements.Add(advertisement);
            db.SaveChanges();

            return advertisement.Id;
        }

        public Advertisement GetAdvertisement(int id)
        {
            return db.Advertisements.Find(id);
        }

        public List<Advertisement> GetAllAdvertisements()
        {

            return parseToList(db.Advertisements);
        }

        public void UpdateAdvertisement(Advertisement advert)
        {
            var advertToUpdate = db.Advertisements.Find(advert.Id);
            db.Entry(advertToUpdate).CurrentValues.SetValues(advert);
            db.SaveChanges();
        }

        public bool DeleteAdvertisement(int id)
        {
            try
            {
                db.Advertisements.Remove(db.Advertisements.Find(id));
                db.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        private static List<Advertisement> parseToList(DbSet<Advertisement> dbset)
        {
            List<Advertisement> advertisements = new List<Advertisement>();

            foreach (var advert in dbset)
            {
                advertisements.Add(advert);
            }

            return advertisements;
        }

    }
}