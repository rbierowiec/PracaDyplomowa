﻿using AdditionalDatas.Contexts;
using AdditionalDatas.Interfaces;
using Library;
using System.Collections.Generic;
using System.Data.Entity;

namespace AdditionalDatas.DbCRUD.PostgreSQL
{
    public class UsersRepository : IUsersRepository
    {
        private MyContext db = new MyContext();

        public void AddUser(User user) {
                db.Users.Add(user);
                db.SaveChanges();
        }

        public User GetUser(int id)
        {
            return db.Users.Find(id);
        }

        public List<User> GetAllUsers() {

            return parseToList(db.Users);
        }

        public void UpdateUser(User user)
        {
            var userToUpdate = db.Users.Find(user.Id);
            db.Entry(userToUpdate).CurrentValues.SetValues(user);
            db.SaveChanges();
        }

        public bool DeleteUser(int id)
        {
            try
            {
                db.Users.Remove(db.Users.Find(id));
                db.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        private static List<User> parseToList(DbSet<User> dbset)
        {
            List<User> users = new List<User>();

            foreach (var user in dbset)
            {
                users.Add(user);
            }

            return users;
        }

    }
}