﻿using AdditionalDatas.Contexts;
using AdditionalDatas.Interfaces;
using Library;
using System.Collections.Generic;
using System.Data.Entity;

namespace AdditionalDatas.DbCRUD.PostgreSQL
{
    public class ApplicationsRepository : IApplicationsRepository
    {
        private MyContext db = new MyContext();

        public int AddApplication(Application app)
        {
            db.Applications.Add(app);
            db.SaveChanges();

            return app.Id;
        }

        public Application GetApplication(int id)
        {
            return db.Applications.Find(id);
        }

        public List<Application> GetAllApplications()
        {

            return parseToList(db.Applications);
        }

        public void UpdateApplication(Application app)
        {
            var appToUpdate = db.Applications.Find(app.Id);
            db.Entry(appToUpdate).CurrentValues.SetValues(app);
            db.SaveChanges();
        }

        public bool DeleteApplication(int id)
        {
            try
            {
                db.Applications.Remove(db.Applications.Find(id));
                db.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        private static List<Application> parseToList(DbSet<Application> dbset)
        {
            List<Application> applications = new List<Application>();

            foreach (var advert in dbset)
            {
                applications.Add(advert);
            }

            return applications;
        }
    }
}