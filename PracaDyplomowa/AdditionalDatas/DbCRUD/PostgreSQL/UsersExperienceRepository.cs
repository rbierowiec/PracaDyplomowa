﻿using AdditionalDatas.Contexts;
using AdditionalDatas.Interfaces;
using Library;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace AdditionalDatas.DbCRUD.PostgreSQL
{
    public class UsersExperienceRepository : IUsersExperienceRepository
    {
        private MyContext db = new MyContext();

        public int AddUserExperience(Experience exp) {
            db.usersExperience.Add(exp);
            db.SaveChanges();

            return exp.Id;
        }

        public Experience GetUserExperience(int id) {
            return db.usersExperience.Find(id);
        }

        public List<Experience> GetAllUserExperience(int userId) {
            return db.usersExperience.Select(n => n).Where(n => n.userId == userId).ToList();
        }

        public void UpdateExperience(Experience exp)
        {
            var expToUpdate = db.usersExperience.Find(exp.Id);
            db.Entry(expToUpdate).CurrentValues.SetValues(exp);
            db.SaveChanges();
        }

        public bool DeleteExperience(int id)
        {
            try
            {
                db.usersExperience.Remove(db.usersExperience.Find(id));
                db.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        private static List<Experience> parseToList(DbSet<Experience> dbset)
        {
            List<Experience> exps = new List<Experience>();

            foreach (var exp in dbset)
            {
                exps.Add(exp);
            }

            return exps;
        }
    }
}