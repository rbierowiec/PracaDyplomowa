﻿using AdditionalDatas.Interfaces;
using Autofac;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac.Integration.WebApi;

namespace AdditionalDatas
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

            var builder = new ContainerBuilder();
            builder.RegisterType<DbCRUD.PostgreSQL.UsersRepository>().As<IUsersRepository>().InstancePerRequest();
            builder.RegisterType<DbCRUD.PostgreSQL.UsersExperienceRepository>().As<IUsersExperienceRepository>().InstancePerRequest();
            builder.RegisterType<DbCRUD.PostgreSQL.AdvertisementsRepository>().As<IAdvertisementsRepository>().InstancePerRequest();
            builder.RegisterType<DbCRUD.PostgreSQL.ApplicationsRepository>().As<IApplicationsRepository>().InstancePerRequest();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
