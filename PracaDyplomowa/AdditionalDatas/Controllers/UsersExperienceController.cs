﻿using AdditionalDatas.Interfaces;
using Library;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdditionalDatas.Controllers
{
    public class UsersExperienceController : ApiController
    {
        private readonly IUsersExperienceRepository db;
        private readonly IUsersRepository dbUsers;

        public UsersExperienceController(IUsersExperienceRepository _db, IUsersRepository _dbUsers)
        {
            db = _db;
            dbUsers = _dbUsers;
        }

        // GET api/usersexperience
        [Authorize]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            var user = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == User.Identity.Name).FirstOrDefault();


            return request.CreateResponse(HttpStatusCode.OK, db.GetAllUserExperience(user.Id));
        }

        // POST api/usersexperience
        [Authorize]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Experience value)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            value.userId = authorizedUser.Id;
            var newItemId = db.AddUserExperience(value);

            return request.CreateResponse(HttpStatusCode.OK, new { id = newItemId });
        }

        // PUT api/usersexperience/5
        [Authorize]
        public HttpResponseMessage Put(int id, HttpRequestMessage request, [FromBody]Experience value)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var editingExp = db.GetUserExperience(id);

            value.userId = editingExp.userId;
            value.type = editingExp.type;

            if (value.userId == authorizedUser.Id)
            {
                db.UpdateExperience(value);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "No authorized action"});
            }
        }

        // DELETE api/usersexperience/5
        [Authorize]
        public HttpResponseMessage Delete(int id, HttpRequestMessage request)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var editingExp = db.GetUserExperience(id);

            if (authorizedUser.Id == editingExp.userId)
            {
                db.DeleteExperience(id);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "No authorized action" });
            }
        }

    }
}