﻿using AdditionalDatas.Interfaces;
using Library;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace AdditionalDatas.Controllers
{
    public class ApplicationsController : ApiController
    {
        private readonly IApplicationsRepository db;
        private readonly IUsersRepository dbUsers;
        private readonly IAdvertisementsRepository dbAdvert;

        public ApplicationsController(IApplicationsRepository _db, IUsersRepository _dbUsers, IAdvertisementsRepository _dbAdvert)
        {
            db = _db;
            dbUsers = _dbUsers;
            dbAdvert = _dbAdvert;
        }

        // GET api/user/applications
        [Authorize]
        [Route("api/user/applications")]
        [HttpGet]
        public HttpResponseMessage GetUserApplications(HttpRequestMessage request)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var filteredApplications = db.GetAllApplications().Select(n => n).Where(n => n.userId == authorizedUser.Id);

            var returnObject = new List<Object>();

            foreach (var app in filteredApplications)
            {
                dynamic item = new ExpandoObject();
                foreach (PropertyInfo propertyInfo in app.GetType().GetProperties())
                {
                    ((IDictionary<string, object>)item)[propertyInfo.Name] = propertyInfo.GetValue(app, null);
                }
                var advert = dbAdvert.GetAdvertisement(app.advertisementId);
                if (advert != null)
                {
                    item.advertisementTitle = advert.Title;
                }
                else {
                    item.advertisementTitle = "Advertisement deleted";
                    item.status = -1;
                }

                returnObject.Add(item);
            }

            return request.CreateResponse(HttpStatusCode.OK, returnObject);
        }

        // GET api/advertisements/applications/X
        [Authorize]
        [Route("api/notices/applications/{id}")]
        [HttpGet]
        public HttpResponseMessage GetApplicationsForAdvert(HttpRequestMessage request, int id)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();

            var advert = dbAdvert.GetAdvertisement(id);
            var filteredApplications = db.GetAllApplications().Select(n => n).Where(n => n.advertisementId == id);

            if (authorizedUser.Id == advert.OwnerId)
            {
                var returnObject = new List<Object>();

                foreach (var app in filteredApplications)
                {
                    dynamic item = new ExpandoObject();
                    foreach (PropertyInfo propertyInfo in app.GetType().GetProperties())
                    {
                        ((IDictionary<string, object>)item)[propertyInfo.Name] = propertyInfo.GetValue(app, null);
                    }
                    var tmp = dbUsers.GetUser(app.userId);

                    if (tmp != null)
                    {
                        item.applicantName = tmp.Surname + " " + tmp.Name;
                    }
                    else
                    {
                        item.applicantName = "User deleted";
                    }

                    returnObject.Add(item);
                }
                return request.CreateResponse(HttpStatusCode.OK, returnObject);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "No authorized action" });
            }
        }

        // POST api/advertisements
        [Authorize]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Application app)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            
            var advert = dbAdvert.GetAdvertisement(app.advertisementId);

            if (advert != null)
            {

                var appFromBase = db.GetAllApplications().Where(n => n.userId == authorizedUser.Id && n.advertisementId == app.advertisementId).FirstOrDefault();

                if (appFromBase == null)
                {
                    app.userId = authorizedUser.Id;
                    app.status = 2;
                    app.dateOfApply = DateTime.Now.ToString();
                    var newItemId = db.AddApplication(app);

                    return request.CreateResponse(HttpStatusCode.OK, new { id = newItemId });
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "You have already send a application for that advertisement" });
                }
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "Advertisement not found" });
            }
        }

        // PUT api/advertisements/5
        [Authorize]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]Application app)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var editingItem = db.GetApplication(id);

            var ownerOfAdvert = dbAdvert.GetAdvertisement(editingItem.advertisementId);

            if (editingItem == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "Item not found" });
            }
            else if (authorizedUser.Id != editingItem.userId && ownerOfAdvert.OwnerId != editingItem.userId)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "No authorized action" });
            }
            else
            {

                if (editingItem.status == 2)
                {
                    editingItem.status = app.status;
                    //app = editingItem;
                    //app.userId = authorizedUser.Id;
                    //app.dateOfApply = editingItem.dateOfApply;
                    //app.Motivation = editingItem.Motivation;

                    if (app.status >4 || app.status < 1) {
                        editingItem.status = 2;
                    }

                    db.UpdateApplication(editingItem);
                    return request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "Cannot modify the application" });
                }
            }
        }



        // DELETE api/advertisements/5
        [Authorize]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var app = db.GetApplication(id);

            if (authorizedUser.Id == app.userId)
            {
                if (app.status == 2)
                {
                    app.status = 3;
                    db.UpdateApplication(app);
                    return request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "Cannot archive the application" });
                }
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "No authorized action" });
            }
        }
    }
}