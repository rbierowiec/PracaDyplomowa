﻿using AdditionalDatas.Interfaces;
using Library;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace AdditionalDatas.Controllers
{
    public class NoticesController : ApiController
    {
        private readonly IAdvertisementsRepository db;
        private readonly IUsersRepository dbUsers;

        public NoticesController(IAdvertisementsRepository _db, IUsersRepository _dbUsers)
        {
            db = _db;
            dbUsers = _dbUsers;
        }

        // GET api/advertisements
        [Authorize]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            var advertisements = db.GetAllAdvertisements();

            var returnObject = new List<Object>();

            foreach (var advert in advertisements) {
                dynamic item = new ExpandoObject();
                foreach (PropertyInfo propertyInfo in advert.GetType().GetProperties())
                {
                    ((IDictionary<string, object>)item)[propertyInfo.Name] = propertyInfo.GetValue(advert, null);
                }

                var author = dbUsers.GetUser(advert.OwnerId);
                if (author != null)
                {
                    item.Author = author.Surname + " " + author.Name;
                }
                else {
                    item.Author = "";
                }
                returnObject.Add(item);
            }

            return request.CreateResponse(HttpStatusCode.OK, returnObject);
        }

        // GET api/advertisements/user
        [Authorize]
        [Route("api/user/advertisements")]
        [HttpGet]
        public HttpResponseMessage GetUserAdvertisements(HttpRequestMessage request)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var filteredAdvertisements = db.GetAllAdvertisements().Select(n => n).Where(n => n.OwnerId == authorizedUser.Id);

            return request.CreateResponse(HttpStatusCode.OK, filteredAdvertisements);
        }

        // GET api/advertisements/5
        [Authorize]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            return request.CreateResponse(HttpStatusCode.OK, db.GetAdvertisement(id));
        }

        // POST api/advertisements
        [Authorize]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Advertisement advert)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();

            if (authorizedUser.Role > 1)
            {
                advert.OwnerId = authorizedUser.Id;
                advert.CreationDate = DateTime.Now.ToString();
                var newItemId = db.AddAdvertisement(advert);

                return request.CreateResponse(HttpStatusCode.OK, new { id = newItemId });
            }
            else {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "Action not allowed" });
            }
        }

        // PUT api/advertisements/5
        [Authorize]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]Advertisement advert)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var editingItem = db.GetAdvertisement(id);

            if (editingItem == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "Item not found" });
            }
            else if (authorizedUser.Id != editingItem.OwnerId)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "No authorized action" });
            }
            else
            {
                advert.OwnerId = authorizedUser.Id;
                advert.CreationDate = editingItem.CreationDate;

                db.UpdateAdvertisement(advert);
                return request.CreateResponse(HttpStatusCode.OK);
            }
        }

        // DELETE api/advertisements/5
        [Authorize]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            var userName = User.Identity.Name;
            var authorizedUser = dbUsers.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();
            var advert = db.GetAdvertisement(id);

            if (authorizedUser.Id == advert.OwnerId)
            {
                db.DeleteAdvertisement(id);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "No authorized action" });
            }
        }
    }
}
