﻿using AdditionalDatas.Interfaces;
using Library;
using System.Linq;
using System.Web.Http;

namespace AdditionalDatas.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUsersRepository db;

        public UsersController(IUsersRepository _db)
        {
            db = _db;
        }

        // GET api/users
        [Authorize]
        public User Get()
        {
            var user = db.GetAllUsers().Select(n=>n).Where(n => n.Email == User.Identity.Name).FirstOrDefault();

            if (user == null) {
                db.AddUser(new User() { Email = User.Identity.Name });
                user = db.GetAllUsers().Select(n => n).Where(n => n.Email == User.Identity.Name).FirstOrDefault();
            }

            return user;
        }

        // GET api/users/5
        [Authorize]
        public User Get(int id)
        {
            return db.GetUser(id);
        }

        // GET api/users/email
        [Authorize]
        public User Get(string email)
        {
            return db.GetAllUsers().Find(u => u.Email == email);
        }

        // POST api/users
        [Authorize]
        public object Post([FromBody]User value)
        {
            var userName = User.Identity.Name;
            var userValidation = isUserValid(value, userName);

            if (!(bool)userValidation.GetType().GetProperty("error").GetValue(userValidation,null))
            {
                db.AddUser(value);
            }

            return userValidation;
        }

        // PUT api/users/5
        [Authorize]
        public object Put(int id, [FromBody]User value)
        {
            var userName = User.Identity.Name;
            var userValidation = isUserValid(value, userName);

            if (!(bool)userValidation.GetType().GetProperty("error").GetValue(userValidation, null))
            {
                db.UpdateUser(value);
            }

            return userValidation;
        }

        // DELETE api/artists/5
        [Authorize]
        public void Delete(int id)
        {
            db.DeleteUser(id);
        }

        private object isUserValid(User user, string userName)
        {
            var authorizedUser = db.GetAllUsers().Select(n => n).Where(n => n.Email == userName).FirstOrDefault();

            if (user == null)
            {
                return new { error = true, msg = "Please fill all required fields." };
            }
            else if (user.Email == "")
            {
                return new { error = true, msg = "Please fill all required fields." };
            }
            else if (authorizedUser.Id != user.Id)
            {
                return new { error = true, msg = "Non authorized action" };
            }
            else
            {
                return new { error = false, msg = userName };
            }
        }
    }
}
