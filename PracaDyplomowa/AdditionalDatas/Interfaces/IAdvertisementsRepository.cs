﻿using Library;
using System.Collections.Generic;

namespace AdditionalDatas.Interfaces
{
    public interface IAdvertisementsRepository
    {
        int AddAdvertisement(Advertisement advertisement);
        Advertisement GetAdvertisement(int id);
        List<Advertisement> GetAllAdvertisements();
        void UpdateAdvertisement(Advertisement advertisement);
        bool DeleteAdvertisement(int id);
    }
}
