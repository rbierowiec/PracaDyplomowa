﻿using Library;
using System.Collections.Generic;

namespace AdditionalDatas.Interfaces
{
    public interface IApplicationsRepository
    {
        int AddApplication(Application app);
        Application GetApplication(int id);
        List<Application> GetAllApplications();
        void UpdateApplication(Application app);
        bool DeleteApplication(int id);
    }
}
