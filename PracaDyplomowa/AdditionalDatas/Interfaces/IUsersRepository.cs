﻿using Library;
using System.Collections.Generic;

namespace AdditionalDatas.Interfaces
{
    public interface IUsersRepository
    {
        void AddUser(User user);
        User GetUser(int id);
        List<User> GetAllUsers();
        void UpdateUser(User user);
        bool DeleteUser(int id);
    }
}
