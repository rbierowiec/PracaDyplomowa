﻿using Library;
using System.Collections.Generic;

namespace AdditionalDatas.Interfaces
{
    public interface IUsersExperienceRepository
    {
        int AddUserExperience(Experience exp);
        Experience GetUserExperience(int id);
        List<Experience> GetAllUserExperience(int userId);
        void UpdateExperience(Experience exp);
        bool DeleteExperience(int id);
    }
}